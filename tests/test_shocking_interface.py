from click.testing import CliRunner
from pytest import fixture, mark

from shocking import __version__
from shocking.interface import cli


@fixture
def cli_runner():
    return CliRunner()


@mark.parametrize("flag", ["-h", "--help"])
def test_cli_option_help(cli_runner, flag):
    result = cli_runner.invoke(cli, [flag])

    assert result.exit_code == 0

    # Check for option flags
    option_header_index = result.output.index("Options:")
    option_section = result.output[option_header_index:result.output.index("\n\n", option_header_index)]

    def check_for_option_flags(short_flag, long_flag):
        assert short_flag in option_section
        assert long_flag in option_section

    check_for_option_flags("-v", "--verbose")
    check_for_option_flags("-d", "--debug")
    check_for_option_flags("-q", "--quiet")
    check_for_option_flags("-h", "--help")
    check_for_option_flags("-V", "--version")

    # Check for commands
    commands_section = result.output[result.output.index("Commands:"):]

    assert "build" in commands_section
    # assert "show" in commands_section
    assert "serve" in commands_section


@mark.parametrize("flag", ["-V", "--version"])
def test_cli_option_version(cli_runner, flag):
    expected = f"shocking, version {__version__}\n"

    result = cli_runner.invoke(cli, [flag])

    assert result.exit_code == 0
    assert result.output == expected
