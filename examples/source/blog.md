---
layout = "page"
author_name = "Ted Moseley"
page_name = "Blog"
---
Here is how you print `Hello, World` in Python 3.

## Code Preview

```python
print("Hello, World!")
```
