from logging import getLogger
from pathlib import Path
from sys import exit

from pkg_resources import iter_entry_points
from tomlkit import loads

from shocking.processor import CopyProcessor, GroupProcessor


class CoreManager:
    def __init__(self, project_path):
        self.logger = getLogger("shocking").getChild("core")

        self.root = Path(project_path)

        self._config = self._get_config()

        self.source_dir = self._get_source_directory()
        self.build_dir = self._get_build_directory()

        self._plugins = self._get_plugins()
        self.processors = self._get_processors()

    """ Iterate a path recursively for file paths """
    @staticmethod
    def _iterate_children(path):
        def iterate_children(path_obj):
            for child in path_obj.iterdir():
                if child.name[0] == '_':
                    continue
                if child.is_file():
                    yield child
                elif child.is_dir():
                    yield from iterate_children(child)

        yield from iterate_children(path)

    """ Iterate an entry point for plugins """
    @staticmethod
    def _iterate_entry_points(entry_point_name):
        for ep in iter_entry_points(group=entry_point_name):
            yield ep.name, ep.load()

    def _get_config(self):
        config_path = self.root.joinpath("pyproject.toml")

        try:
            raw_config = loads(config_path.read_text())
        except FileNotFoundError:
            self.logger.error("Could not find configuration file '%s'. Exiting", config_path)
            exit(1)
        else:
            self.logger.debug("Configuration file '%s' opened successfully", config_path)

        try:
            return raw_config["tool"]["shocking"]
        except KeyError:
            self.logger.error("'tool.shocking' not defined in '%s'", config_path)
            exit(1)

    def _get_source_directory(self):
        source_path = self.root.joinpath(self._config.get("source", "source"))

        if not source_path.is_dir():
            self.logger.error("Source directory '%s' does not exist", source_path)
            exit(1)

        self.logger.debug("Source directory is '%s'", source_path)

        return source_path

    def _get_build_directory(self):
        build_path = self.root.joinpath(self._config.get("build", "build"))

        if not build_path.is_dir():
            self.logger.info("Creating build directory '%s'", build_path)
            build_path.mkdir(parents=True)

        self.logger.debug("Build directory is '%s'", build_path)

        return build_path

    def _get_plugins(self):
        """ Initialize a plugin with config values """
        def init_plugin(name, module):
            self.logger.debug("Configuring plugin '%s'...", name)

            return module(
                name,
                source=self.source_dir,
                build=self.build_dir,
                # The configuration defined for the plugin
                **self._config.get("plugin", {}).get(name, {})
            )

        self.logger.debug("Loading plugins from entry point")

        plugin_dict = {
            name: init_plugin(name, module)
            for name, module in self._iterate_entry_points("shocking.plugin")
        }

        # If no plugins installed, shocking does nothing
        if not plugin_dict:
            self.logger.error("No plugins installed. Exiting")
            exit(1)

        return plugin_dict

    def _get_processors(self):
        try:
            processing_groups = self._config["group"]
        except KeyError:
            self.logger.error("No processing groups defined. Exiting")
            exit(1)

        try:
            group_processors = [
                GroupProcessor(self.source_dir, self.build_dir, self._plugins, group)
                for group in processing_groups
            ]
        except RuntimeError as e:
            self.logger.error("Error with forming group: '%s'", e)

        processor_dict = {
            file_path: processor
            for processor in group_processors
            for file_path in processor.files
        }

        copy_processor = CopyProcessor(self.source_dir, self.build_dir)

        for file_path in self._iterate_children(self.source_dir):
            if file_path not in processor_dict:
                processor_dict[file_path] = copy_processor

        return processor_dict
