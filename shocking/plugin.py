from logging import getLogger

import pkg_resources


class BasePlugin():
    """ A base class for shocking plugins. """

    def __init__(self, name, **config):
        """ Do not override this method in subclasses! """
        self.logger = getLogger("shocking").getChild(name)

        try:
            self.config = dict(self._defaults)
            self.config.update(config)
        except AttributeError:
            self.config = dict(config)
        finally:
            self.prepare()

    def prepare(self):
        """
        An optional method responsible for the preparation of the
        plugin. Typically this method is used to manipulate the
        initial configuration values in some way that can save time
        later on in the process stage. Also a good chance to prevent
        any errors that might occur.
        """
        pass

    def process(self, file_object, **metadata):
        """
        Process an individual file. Always returns a file_object
        back, modified or not.

        Common practice is to check for a valid file extension
        first (using ``file_object.path.suffix``) if applicable.

        After processing occurs, if the content or path are changed,
        it is common to use the file_object's ``_replace`` method
        (see namedtuple).
        """
        pass


def print_names(entry_point_name):
    for ep in pkg_resources.iter_entry_points(group=entry_point_name):
        print(ep.name)


def iterate(entry_point_name):
    for ep in pkg_resources.iter_entry_points(group=entry_point_name):
        yield ep.name, ep.load()
